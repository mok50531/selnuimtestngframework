package scripts;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.Test;
import utilities.Waiter;

import java.util.List;

public class CarvanaPageTest extends Base{

    @Test(testName = "Validate Carvana home page title and url", priority = 1)
    public void validateCarvanaHomePageTitleAndURL(){
        /*
        Given user is on "https://www.carvana.com/"
        Then validate title equals to "Carvana | Buy & Finance Used Cars Online | At Home Delivery"
        And validate url equals to "https://www.carvana.com/"
         */
        driver.get("https://www.carvana.com/");

        Assert.assertEquals(driver.getTitle(), "Carvana | Buy & Finance Used Cars Online | At Home Delivery");
        Assert.assertEquals(driver.getCurrentUrl(), "https://www.carvana.com/");
    }

    @Test(testName = "Validate the Carvana logo", priority = 2)
    public void validateTheCarvanaLogo(){
        /*
        Given user is on "https://www.carvana.com/"
        Then validate the logo of the “Carvana” is displayed
         */
        driver.get("https://www.carvana.com/");

        Assert.assertTrue(driver.findElement(By.cssSelector(".fSZhVx")).isDisplayed());
    }

    @Test(testName = "Validate the main navigation section items", priority = 3)
    public void validateTheMainNavigationSectionItems(){
        /*
        Given user is on "https://www.carvana.com/"
        Then validate the navigation section items below are displayed
        |HOW IT WORKS      |
        |ABOUT CARVANA     |
        |SUPPORT & CONTACT |
         */

        driver.get("https://www.carvana.com/");

        Assert.assertTrue(driver.findElement(By.cssSelector(".kQizBt")).isDisplayed());
        Assert.assertTrue(driver.findElement(By.cssSelector(".eNcYVl")).isDisplayed());
        Assert.assertTrue(driver.findElement(By.cssSelector(".hAARzw")).isDisplayed());

    }

    @Test(testName = "Validate the main navigation sections items", priority = 4)
    public void validateTheMainNavigationSectionsItems(){
        /*
        Given user is on "https://www.carvana.com/"
        When user clicks on “SIGN IN” button
        Then user should be navigated to “Sign in” modal
        When user enters email as “johndoe@gmail.com”
        And user enters password as "abcd1234"
        And user clicks on "SIGN IN" button
        Then user should see error message as "Email address and/or password combination is incorrect
        Please try again or reset your password."
         */

        driver.get("https://www.carvana.com/");

        driver.findElement(By.cssSelector("a[role='button']")).click();

        Assert.assertTrue(driver.findElement(By.cssSelector(".jFXgER")).isDisplayed());

        driver.findElement(By.id("usernameField")).sendKeys("jogndoe@gmail.com");
        driver.findElement(By.id("passwordField")).sendKeys("abcd1234");

        driver.findElement(By.xpath("//button[text()='Sign In']")).click();

        Assert.assertTrue(driver.findElement(By.cssSelector(".dhUWhc")).isDisplayed());

    }

    @Test(testName = "Validate the search filter options and search button", priority = 5)
    public void validateTheSearchFilterOptionsAndSearchButton(){
        /*
        Given user is on "https://www.carvana.com/"
        When user clicks on "SEARCH ALL VEHICLES" link
        Then user should be routed to "https://www.carvana.com/cars"
        And user should see a search input box
        And user should see filter options
        |PAYMENT & PRICE      |
        |MAKE & MODEL      |
        |BODY TYPE |
        |YEAR & MILEAGE      |
        |FEATURES      |
        |MORE FILTERS |
        When user enters "Tesla" to the search input box
        Then validate "GO" button in the search input box is displayed as expected
         */
        driver.get("https://www.carvana.com/");
        WebElement searchAllVehicles = driver.findElement(By.xpath("//a[text()='search all vehicles']/parent::div"));
        Waiter.waitForWebElementToBeClickable(driver, 30, searchAllVehicles);
        searchAllVehicles.click();
        Waiter.pause(15);
        Assert.assertEquals(driver.getCurrentUrl(), "https://www.carvana.com/cars");
        Assert.assertTrue(driver.findElement(By.cssSelector(".react-autosuggest__input")).isDisplayed());
        List<WebElement> carOptions = driver.findElements(By.cssSelector("div[data-qa='menu-flex']>button"));

        for (WebElement carOption : carOptions) {
            Assert.assertTrue(carOption.isDisplayed());
            System.out.println(carOption.getText());
        }

        driver.findElement(By.cssSelector(".react-autosuggest__input")).sendKeys("Tesla");
        Assert.assertTrue(driver.findElement(By.xpath("//button[text()='GO']")).isDisplayed());
    }

    @Test(testName = "Validate the search result tiles", priority = 6)
    public void validateTheSearchResultTiles(){
        /*
        Given user is on "https://www.carvana.com/"
        When user clicks on "SEARCH ALL VEHICLES" link
        Then user should be routed to "https://www.carvana.com/cars"
        When user enters "mercedes-benz" to the search input box
        And user clicks on "GO" button in the search input box
        Then user should see "mercedes-benz" in the url
        And validate each result tile
        VALIDATION OF EACH TILE INCLUDES BELOW
        Make sure each result tile is displayed with below information
        1. an image
        2. add to favorite button
        3. tile body
        ALSO VALIDATE EACH TILE BODY:
        Make sure each tile body has below information
        1. Inventory type - text should be displayed and should not be null
        2. Year-Make-Model information - text should be displayed and should not be null
        3. Trim-Mileage information - text should be displayed and should not be null
        4. Price - Make sure that each price is more than zero
        5. Trim-Mileage information - text should be displayed and should not be null
        6. Monthly Payment information - text should be displayed and should not be null
        7. Down Payment information - text should be displayed and should not be null
        8. Delivery chip must be displayed as “Free Shipping
         */

        driver.get("https://www.carvana.com/");

        WebElement searchAllVehicles = driver.findElement(By.xpath("//a[text()='search all vehicles']/parent::div"));
        Waiter.waitForWebElementToBeClickable(driver, 30, searchAllVehicles);

        searchAllVehicles.click();

        driver.findElement(By.cssSelector(".react-autosuggest__input")).sendKeys("mercedes-benz");
        driver.findElement(By.cssSelector("button[class='KeywordSuggestionsstyles__GoButton-hxofhy-4 iPPBRN']")).click();

        Waiter.pause(10);

        Assert.assertTrue(driver.getCurrentUrl().contains("mercedes-benz"));

        List<WebElement> allCars = driver.findElements(By.cssSelector("#results-section>div[class='result-tile']"));
        List<WebElement> carsInventoryTypes = driver.findElements(By.cssSelector(".inventory-type-experiment"));
        List<WebElement> carsMakesAndModels = driver.findElements(By.cssSelector(".make-model-experiment"));
        List<WebElement> carsMileagesAndTrims = driver.findElements(By.cssSelector(".trim-mileage"));
        List<WebElement> carsPrices = driver.findElements(By.cssSelector(".middle-frame-pane>div[class='price-variant ']"));
        List<WebElement> carsMonthlyPayments = driver.findElements(By.cssSelector(".term-row>div[class='monthly-payment']"));
        List<WebElement> carsDownPayments = driver.findElements(By.cssSelector(".term-row>div[class='down-payment']"));
        List<WebElement> carsDeliveryOptions = driver.findElements(By.cssSelector(".base-delivery-chip"));

        for (WebElement allCar : allCars) {
            Assert.assertTrue(allCar.isDisplayed());
        }

        for (WebElement carsInventoryType : carsInventoryTypes) {
            Assert.assertTrue(carsInventoryType.isDisplayed() && !carsInventoryType.getText().isEmpty());
        }

        for (WebElement carsMakesAndModel : carsMakesAndModels) {
            Assert.assertTrue(carsMakesAndModel.isDisplayed() && !carsMakesAndModel.getText().isEmpty());
        }

        for (WebElement carsMileagesAndTrim : carsMileagesAndTrims) {
            Assert.assertTrue(carsMileagesAndTrim.isDisplayed() && !carsMileagesAndTrim.getText().isEmpty());
        }

        for (WebElement carsPrice : carsPrices) {
            Assert.assertTrue(carsPrice.isDisplayed());
            Assert.assertTrue(Integer.parseInt(carsPrice.getText().replaceAll("[^0-9]", "")) > 0);
        }

        for (WebElement carsMonthlyPayment : carsMonthlyPayments) {
            Assert.assertTrue(carsMonthlyPayment.isDisplayed() && !carsMonthlyPayment.getText().isEmpty());
        }

        for (WebElement carsDownPayment : carsDownPayments) {
            Assert.assertTrue(carsDownPayment.isDisplayed() && !carsDownPayment.getText().isEmpty());
        }

        for (WebElement carsDeliveryOption : carsDeliveryOptions) {
            Assert.assertTrue(carsDeliveryOption.isDisplayed() && carsDeliveryOption.getText().equals("Free Shipping"));
        }

    }
}
